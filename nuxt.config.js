// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  runtimeConfig: {
    public: {
      apiHost: "http://localhost:3001"
    }
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  // meta: {
  //   title: 'myegane49',
  //   // htmlAttrs: {
  //   //   lang: 'en'
  //   // },
  //   meta: [
  //     { charset: 'utf-8' },
  //     { name: 'viewport', content: 'width=device-width, initial-scale=1' },
  //     // { hid: 'description', name: 'description', content: '' },
  //     // { name: 'format-detection', content: 'telephone=no' }
  //   ],
  //   link: [
  //     { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
  //   ]
  // },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/styles/base.scss',
    '@fortawesome/fontawesome-svg-core/styles.css'
  ],

  build: { transpile: ['@fortawesome/vue-fontawesome'], },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  // plugins: [
  //   '~plugins/lang-mixin.js'
  // ],

  // styleResources: {
  //   scss: [
  //     './styles/_variables.scss',
  //   ]
  // },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    // '@nuxtjs/axios',
    // '@nuxtjs/style-resources',
    // '@nuxtjs/fontawesome',
    '@pinia/nuxt'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  // axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    // baseURL: '/',
    // baseURL: 'http://localhost:3000/',
    // baseURL: 'https://myegane49.xyz/',
  // },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  // build: {
  // },

  // router: {
  //   middleware: 'readLang'
  // },

  // serverMiddleware: ['./server'],
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@use "@/assets/styles/_variables.scss" as *;'
        }
      }
    }
  }
})
