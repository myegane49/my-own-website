import { useLangStore } from '@/store/index'

export default defineNuxtRouteMiddleware((to, from) => {
  const store = useLangStore()
  let cookies = useRequestHeaders(['cookie']).cookie
  if (cookies !== undefined && cookies.includes('lang=')) {
    const lang = cookies.split(';').find(c => c.trim().startsWith('lang=')).split('=')[1]
    store.setLang(lang, true)
  }
})