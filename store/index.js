import { defineStore } from 'pinia'

const language = {
  state: () => ({lang: 'fa'}),

  actions: {
    setLang(payload, server) {
      if (!server) {
        document.cookie = `lang=${payload}; SameSite=None; Secure`
      }
      this.lang = payload
    }
  }
}

export const useLangStore = defineStore('lang', language)
