import { ref, computed } from 'vue'
import { useLangStore } from '@/store/index'

export const useLang = () => {
  const langStore = useLangStore()

  const persianText = ref(null)
  const englishText = ref(null)

  const text = computed(() => {
    return langStore.lang === 'en' ? englishText.value : persianText.value
  })

  return {persianText, englishText, text}
}
