import 'dotenv/config'

import express from 'express'
import { MongoClient, ObjectId } from 'mongodb'

let db = null
MongoClient.connect(process.env.MONGODB_URL).then(client => {
  db = client.db(process.env.MONGODB_DB)
})

const app = express()

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE')
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization')
  next()
})

app.get('/posts/:page', async (req, res) => {
  const page = req.params.page
  const searchPhrase = req.query.search
  const postPerPage = +process.env.POST_PER_PAGE
  const skip = Math.ceil((page - 1) * postPerPage)

  let posts, totalCount
  if (searchPhrase) {
    const regExp = new RegExp(searchPhrase)
    posts = await db.collection('posts').find({$or: [
      {'title.en': {$regex: regExp}},
      {'title.fa': {$regex: regExp}},
      {'contents.fa': {$regex: regExp}},
      {'contents.en': {$regex: regExp}}
    ]}).sort({createdAt: -1}).skip(skip).limit(postPerPage).toArray()
    totalCount = await db.collection('posts').count({$or: [
      {'title.en': {$regex: regExp}},
      {'title.fa': {$regex: regExp}},
      {'contents.fa': {$regex: regExp}},
      {'contents.en': {$regex: regExp}}
    ]})
  } else {
    posts = await db.collection('posts').find().sort({createdAt: -1}).skip(skip).limit(postPerPage).toArray()
    totalCount = await db.collection('posts').countDocuments()
  }
  res.send({posts, totalCount, postPerPage})
})

app.get('/singlePost/:id', async (req, res) => {
  const id = req.params.id
  const post = await db.collection('posts').findOne({_id: new ObjectId(id)})
  res.send(post)
})

app.listen(3001, () => {
  console.log('server running on 3001')
})
