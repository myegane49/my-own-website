import { library, config } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faHouse, faLanguage, faLink, faBlog, faMagnifyingGlass, faCircleRight, faCircleLeft } from '@fortawesome/free-solid-svg-icons'

library.add(faHouse, faLanguage, faLink, faBlog, faMagnifyingGlass, faCircleRight, faCircleLeft)
config.autoAddCss = false

export default defineNuxtPlugin(nuxtApp => {
  nuxtApp.vueApp.component('font-awesome-icon', FontAwesomeIcon)
})